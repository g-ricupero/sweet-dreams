\version "2.18.2"

\paper {
	#(set-paper-size "a4")          % paper size
	% ragged-last-bottom = ##f      % fill the page till bottom
	% page-count = 1                % force to render just in 1 page
	% between-system-padding = 0    % empty padding between lines
}

NoChords = {
	\override Score.MetronomeMark #'padding = #4
}
WithChords = {
	\override Score.MetronomeMark #'padding = #8
}

\header {
	title = \markup \center-align {
		\override #'(font-name . "Purisa")
		\fontsize #4 \bold
		"Sweet Dreams (Are Made of This)"
	}
	subsubtitle = \markup \center-align {
		\override #'(font-name . "Arial")
		"Soul Rebel Version"
	}
	composer=\markup \center-align {
		\override #'(font-name . "Arial")
		"Annie Lennox, David Stewart"
	}
	opus = \markup \tiny {
		\override #'(font-name . "Arial")
		"(1983)"
	}
	tagline = \markup {
		\override #'(font-name . "Arial")
		\tiny \column {
			\fill-line { "Transcription" \italic { "Giuseppe Ricupero" } }
			\fill-line { "Updated" \italic { "29-11-2018 22.11" } }
		}
	}
}

global = {
	\time 4/4
	\key c \minor
	\tempo 4 = 120
	\set Score.skipBars = ##t
	\set Score.countPercentRepeats = ##t
}
